package `in`.technowolf.anahaassignment.utils

import `in`.technowolf.anahaassignment.R
import android.annotation.SuppressLint
import android.content.Context
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.CandleEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import com.github.mikephil.charting.utils.Utils
import com.google.android.material.imageview.ShapeableImageView
import com.google.android.material.textview.MaterialTextView

@SuppressLint("ViewConstructor")
class CustomChartMarker(context: Context, layoutResource: Int, @DrawableRes private val backgroundDrawable: Int) :
    MarkerView(context, layoutResource) {

    private val tvMarkerView: MaterialTextView = findViewById(R.id.tvContent)
    private val backgroundImage: AppCompatImageView = findViewById(R.id.appCompatImageView)

    override fun refreshContent(entry: Entry, highlight: Highlight) {
        if (entry is CandleEntry) {
            tvMarkerView.text = Utils.formatNumber(entry.high, 0, true)
        } else {
            tvMarkerView.text = Utils.formatNumber(entry.y, 0, true)
        }
        backgroundImage.setBackgroundResource(backgroundDrawable)
        super.refreshContent(entry, highlight)
    }

    override fun getOffset(): MPPointF {
        return MPPointF(
            ((width / 2).unaryMinus()).toFloat(),
            (height.unaryMinus()).toFloat()
        )
    }

}

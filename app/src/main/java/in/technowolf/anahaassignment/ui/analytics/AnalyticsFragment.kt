package `in`.technowolf.anahaassignment.ui.analytics

import `in`.technowolf.anahaassignment.R
import `in`.technowolf.anahaassignment.databinding.FragmentAnalyticsBinding
import `in`.technowolf.anahaassignment.utils.CustomBarchartRenderer
import `in`.technowolf.anahaassignment.utils.CustomChartMarker
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet

class AnalyticsFragment : Fragment() {

    private var _binding: FragmentAnalyticsBinding? = null
    private val binding: FragmentAnalyticsBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAnalyticsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUi()
    }

    private fun setupUi() {
        setupProfileView()
        setupBloodPressureChart()
        setupHeartRateChart()
        setupBodyTemperatureChart()
    }

    private fun setupProfileView() {
        val radius = resources.getDimension(R.dimen.profile_pic_corners)
        binding.ivProfilePic.shapeAppearanceModel = binding.ivProfilePic.shapeAppearanceModel
            .toBuilder()
            .setAllCornerSizes(radius)
            .build()
    }

    private fun setupBloodPressureChart() {
        val customChartMarker = CustomChartMarker(
            requireContext(),
            R.layout.layout_custom_marker,
            R.drawable.ic_baseline_circle_blood_pressure
        )
        binding.barChartBloodPressure.apply {
            renderer = CustomBarchartRenderer(this, animator,viewPortHandler)

            description.isEnabled = false
            setMaxVisibleValueCount(60)
            setPinchZoom(false)
            setDrawBarShadow(false)
            setDrawGridBackground(false)

            customChartMarker.chartView = this
            marker = customChartMarker

            xAxis.isEnabled = false
            xAxis.setDrawGridLines(false)

            axisLeft.setDrawGridLines(false)

            binding.barChartBloodPressure.axisLeft.isEnabled = false
            binding.barChartBloodPressure.axisRight.isEnabled = false

            animateY(1500)

            legend.isEnabled = false
        }
        setupBloodPressureData()
    }

    private fun setupBloodPressureData() {
        val barChartEntries = mutableListOf<BarEntry>()
        val barDataSet: BarDataSet?
        for(i in 1..10){
            var value = (Math.random() * (100 + 1)) + 20
            if (value < 30) value += 10
            barChartEntries.add(BarEntry(i.toFloat(), value.toFloat()))
        }
        if(binding.barChartBloodPressure.data!=null && binding.barChartBloodPressure.data.dataSetCount>0){
            barDataSet = binding.barChartBloodPressure.data.getDataSetByIndex(0) as BarDataSet
            barDataSet.values = barChartEntries
            binding.barChartBloodPressure.data.notifyDataChanged()
            binding.barChartBloodPressure.notifyDataSetChanged()
        } else{
            barDataSet = BarDataSet(barChartEntries,"Blood Pressure")
            barDataSet.color = ContextCompat.getColor(requireContext(),R.color.blood_pressure)
            barDataSet.setDrawValues(false)

            val dataSets = mutableListOf<IBarDataSet>()
            dataSets.add(barDataSet)

            val barData = BarData(dataSets)
            binding.barChartBloodPressure.data = barData
            binding.barChartBloodPressure.setFitBars(true)
        }
        binding.barChartBloodPressure.invalidate()
    }

    private fun setupHeartRateChart() {
        val customChartMarker = CustomChartMarker(
            requireContext(),
            R.layout.layout_custom_marker,
            R.drawable.ic_baseline_circle_heartrate
        )
        binding.lineChartHeartRate.apply {
            description.isEnabled = false
            setTouchEnabled(true)
            isDragEnabled = false
            setScaleEnabled(false)
            customChartMarker.chartView = this
            marker = customChartMarker
            setPinchZoom(false)
            setDrawGridBackground(false)
            maxHighlightDistance = 300f

            val xAxis: XAxis = binding.lineChartHeartRate.xAxis
            xAxis.isEnabled = false

            val y: YAxis = binding.lineChartHeartRate.axisLeft
            y.isEnabled = false

            axisLeft.isEnabled = false
            axisRight.isEnabled = false


            legend.isEnabled = false
            animateX(500, Easing.EaseOutQuad)
            // Invalidate graph each time UI is drawn.
            invalidate()
        }
        setHeartRateData()
    }

    private fun setHeartRateData() {
        val heartRateEntries = mutableListOf<Entry>()
        val lineDataSet: LineDataSet?
        for (i in 1..10) {
            var value = (Math.random() * (72 + 1)) + 20
            if (value < 30) value += 10
            heartRateEntries.add(Entry(i.toFloat(), value.toFloat()))
        }
        if (binding.lineChartHeartRate.data != null && binding.lineChartHeartRate.data.dataSetCount > 0) {
            lineDataSet = binding.lineChartHeartRate.data.getDataSetByIndex(0) as LineDataSet
            lineDataSet.values = heartRateEntries
            binding.lineChartHeartRate.data.notifyDataChanged()
            binding.lineChartHeartRate.notifyDataSetChanged()
        } else {
            lineDataSet = LineDataSet(heartRateEntries, "Heart Rate Data")
            lineDataSet.apply {
                mode = LineDataSet.Mode.CUBIC_BEZIER
                cubicIntensity = 0.2f
                setDrawFilled(true)
                setDrawCircles(false)
                lineWidth = 1.8f
                circleRadius = 1f
                setCircleColor(ContextCompat.getColor(requireContext(), R.color.heart_rate))
                highLightColor = Color.rgb(244, 117, 117)
                color = Color.RED
                fillDrawable =
                    ContextCompat.getDrawable(requireContext(), R.drawable.heart_rate_gradient)
                fillAlpha = 100
                setDrawHorizontalHighlightIndicator(false)
                fillFormatter =
                    IFillFormatter { _, _ ->
                        binding.lineChartHeartRate.axisLeft.axisMinimum
                    }
            }
            val lineData = LineData(lineDataSet)
            lineData.setValueTextSize(9f)
            lineData.setDrawValues(false)
            binding.lineChartHeartRate.data = lineData
        }

    }

    private fun setupBodyTemperatureChart() {
        val customChartMarker = CustomChartMarker(
            requireContext(),
            R.layout.layout_custom_marker,
            R.drawable.ic_baseline_circle_body_temprature
        )
        binding.lineChartBodyTemperature.apply {
            description.isEnabled = false
            setTouchEnabled(true)
            isDragEnabled = false
            setScaleEnabled(false)
            customChartMarker.chartView = this
            marker = customChartMarker
            setPinchZoom(false)
            setDrawGridBackground(false)
            maxHighlightDistance = 300f

            val xAxis: XAxis = binding.lineChartBodyTemperature.xAxis
            xAxis.isEnabled = false

            val y: YAxis = binding.lineChartBodyTemperature.axisLeft
            y.isEnabled = false

            axisLeft.isEnabled = false
            axisRight.isEnabled = false


            legend.isEnabled = false
            animateX(500, Easing.EaseOutQuad)
            // Invalidate graph each time UI is drawn.
            invalidate()
        }
        setBodyTemperatureData()
    }

    private fun setBodyTemperatureData() {
        val bodyTemperatureEntries = mutableListOf<Entry>()
        val lineDataSet: LineDataSet?
        for (i in 1..10) {
            var value = (Math.random() * (36 + 1)) + 20
            if (value < 10) value += 20
            bodyTemperatureEntries.add(Entry(i.toFloat(), value.toFloat()))
        }
        if (binding.lineChartBodyTemperature.data != null && binding.lineChartBodyTemperature.data.dataSetCount > 0) {
            lineDataSet = binding.lineChartBodyTemperature.data.getDataSetByIndex(0) as LineDataSet
            lineDataSet.values = bodyTemperatureEntries
            binding.lineChartBodyTemperature.data.notifyDataChanged()
            binding.lineChartBodyTemperature.notifyDataSetChanged()
        } else {
            lineDataSet = LineDataSet(bodyTemperatureEntries, "Body Temperature Data")
            lineDataSet.apply {
                mode = LineDataSet.Mode.CUBIC_BEZIER
                cubicIntensity = 0.2f
                setDrawFilled(true)
                setDrawCircles(false)
                lineWidth = 1.8f
                circleRadius = 1f
                setCircleColor(ContextCompat.getColor(requireContext(), R.color.body_temperature))
                highLightColor = Color.rgb(244, 117, 117)
                color = Color.RED
                fillDrawable =
                    ContextCompat.getDrawable(requireContext(), R.drawable.body_temperature_gradient)
                fillAlpha = 100
                setDrawHorizontalHighlightIndicator(false)
                fillFormatter =
                    IFillFormatter { _, _ ->
                        binding.lineChartBodyTemperature.axisLeft.axisMinimum
                    }
            }
            val lineData = LineData(lineDataSet)
            lineData.setValueTextSize(9f)
            lineData.setDrawValues(false)
            binding.lineChartBodyTemperature.data = lineData
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
package `in`.technowolf.anahaassignment.data.sentim
import androidx.annotation.Keep

import com.squareup.moshi.JsonClass

import com.squareup.moshi.Json


@Keep
@JsonClass(generateAdapter = true)
data class SentimPRQ(
    @Json(name = "text")
    val text: String
)

@Keep
@JsonClass(generateAdapter = true)
data class SentimRS(
    @Json(name = "result")
    val result: Result,
    @Json(name = "sentences")
    val sentences: List<Sentence>
)

@Keep
@JsonClass(generateAdapter = true)
data class Result(
    @Json(name = "polarity")
    val polarity: Double,
    @Json(name = "type")
    val type: String
)

@Keep
@JsonClass(generateAdapter = true)
data class Sentence(
    @Json(name = "sentence")
    val sentence: String,
    @Json(name = "sentiment")
    val sentiment: Sentiment
)

@Keep
@JsonClass(generateAdapter = true)
data class Sentiment(
    @Json(name = "polarity")
    val polarity: Double,
    @Json(name = "type")
    val type: String
)
package `in`.technowolf.anahaassignment.di

import `in`.technowolf.anahaassignment.data.sentim.SentimService
import `in`.technowolf.anahaassignment.ui.analytics.ApiDemoViewModel
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import okhttp3.OkHttpClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

const val SentimApi = "SentimApi"

val retrofitModule = module {
    single { okHttpProvider(get()) }
    single(named(SentimApi)) { retrofitProviderForSentimApi(get()) }
}

val repoModule = module {
    single { get<Retrofit>(named(SentimApi)).create(SentimService::class.java) }
}

val viewModelModule = module {
    viewModel { ApiDemoViewModel(get()) }
}

val appModule = module {
    single {
        ChuckerInterceptor.Builder(get())
            .collector(get())
            .maxContentLength(250000L)
            .alwaysReadResponseBody(false)
            .build()
    }
    single {
        ChuckerCollector(context = get(), showNotification = true)
    }
}

private fun okHttpProvider(chuckerInterceptor: ChuckerInterceptor): OkHttpClient {
    val okHttpClient = OkHttpClient.Builder()
    okHttpClient.addInterceptor(chuckerInterceptor)
    return okHttpClient.build()
}

private fun retrofitProviderForSentimApi(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl("https://sentim-api.herokuapp.com/api/")
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
}

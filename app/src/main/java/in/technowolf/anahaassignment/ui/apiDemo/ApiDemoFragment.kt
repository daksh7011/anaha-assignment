package `in`.technowolf.anahaassignment.ui.apiDemo

import `in`.technowolf.anahaassignment.data.sentim.SentimPRQ
import `in`.technowolf.anahaassignment.databinding.FragmentApiDemoBinding
import `in`.technowolf.anahaassignment.ui.analytics.ApiDemoViewModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ApiDemoFragment : Fragment() {

    private var _binding: FragmentApiDemoBinding? = null
    private val binding: FragmentApiDemoBinding get() = _binding!!

    private val apiDemoViewModel: ApiDemoViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentApiDemoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUi()
    }

    private fun setupUi() {
        setupEditTexts()
        setupButtons()
        setupObservers()
    }

    private fun setupEditTexts() {
        binding.etText0.doAfterTextChanged {
            binding.tilText0.error = null
        }
    }

    private fun setupButtons() {
        binding.btnDetectSentiment.setOnClickListener {
            binding.apply {
                if (etText0.text.toString() != "") {
                    val sentimPRQ = SentimPRQ(etText0.text.toString())
                    apiDemoViewModel.checkForSentiment(sentimPRQ)
                } else {
                    validateEditTexts()
                }
            }
        }
    }

    private fun validateEditTexts() {
        binding.apply {
            if (etText0.text.toString().isEmpty()) {
                tilText0.error = "This field is required"
            }
        }
    }

    private fun setupObservers() {
        apiDemoViewModel.imageFlipRS.observe(viewLifecycleOwner,{
            val sentimentPolarity = it.result.polarity
            val sentimentType = it.result.type

            val sentenceList = mutableListOf<String>()
            it.sentences.forEach {
                sentenceList.add("Sentence: ${it.sentence}\nSentence Sentiment: ${it.sentiment.type}\nSentence Polarity: ${it.sentiment.polarity}\n")
            }
            val finalString = "Overall Sentiment Result: $sentimentType with polarity $sentimentPolarity\n"
            binding.tvSentimResult.text = finalString
            sentenceList.forEach {
                binding.tvSentimResult.append("\n$it\n")
            }
        })
    }

}
package `in`.technowolf.anahaassignment.ui.home

import `in`.technowolf.anahaassignment.R
import `in`.technowolf.anahaassignment.databinding.FragmentHomeBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUi()
    }

    private fun setupUi() {

        setupProfileView()
        setupButtons()
    }

    private fun setupButtons() {
        binding.btnToAnalytics.setOnClickListener {
            findNavController().navigate(R.id.actionToAnalyticsFragment)
        }
        binding.btnToApiDemo.setOnClickListener {
            findNavController().navigate(R.id.actionToApiDemoFragment)
        }
    }

    private fun setupProfileView() {
        val radius = resources.getDimension(R.dimen.profile_pic_corners)
        binding.ivProfilePic.shapeAppearanceModel = binding.ivProfilePic.shapeAppearanceModel
            .toBuilder()
            .setAllCornerSizes(radius)
            .build()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
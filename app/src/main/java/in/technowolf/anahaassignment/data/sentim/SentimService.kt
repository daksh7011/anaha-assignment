package `in`.technowolf.anahaassignment.data.sentim

import retrofit2.http.Body
import retrofit2.http.POST

interface SentimService {
    @POST("v1")
    suspend fun createMeme(@Body sentimPRQ: SentimPRQ): SentimRS
}
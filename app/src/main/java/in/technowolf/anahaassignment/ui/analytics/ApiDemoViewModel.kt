package `in`.technowolf.anahaassignment.ui.analytics

import `in`.technowolf.anahaassignment.data.sentim.*
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class ApiDemoViewModel(private val sentimService: SentimService) : ViewModel() {

    private val _imageFlipRS = MutableLiveData<SentimRS>()
    val imageFlipRS: LiveData<SentimRS> = _imageFlipRS

    fun checkForSentiment(sentimPRQ: SentimPRQ) {
        viewModelScope.launch {
            _imageFlipRS.value = sentimService.createMeme(sentimPRQ)
        }
    }
}
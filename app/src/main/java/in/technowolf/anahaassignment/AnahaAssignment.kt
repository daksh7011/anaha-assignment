package `in`.technowolf.anahaassignment

import `in`.technowolf.anahaassignment.di.appModule
import `in`.technowolf.anahaassignment.di.repoModule
import androidx.appcompat.app.AppCompatDelegate
import `in`.technowolf.anahaassignment.di.retrofitModule
import `in`.technowolf.anahaassignment.di.viewModelModule
import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AnahaAssignment: Application() {

    override fun onCreate() {
        super.onCreate()

        // Forcefully setting Light mode since Dark mode theme is not being handled.
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        startKoin {
            // Android context
            androidContext(this@AnahaAssignment)
            // Modules
            modules(listOf(
                retrofitModule,
                repoModule,
                viewModelModule,
                appModule
            ))
        }
    }

}